Zabbix in Docker
========================

## Container

Zabbix in Docker 基于 *Zabbix Services*, 查看 [Zabbix documentation](http://www.zabbix.com/) 以获取更多关于Zabbix的内容.

如果你使用本镜像创建了 *Zabbix in Docker* 容器，那么你的容器中有以下内容：

* *Zabbix Server* 监听端口 10051.
* *Zabbix Java Gateway* 监听端口 10052.
* *Zabbix Web UI* 监听端口 80 (e.g. `http://$container_ip/zabbix` user : admin, passwd : zabbix)
* *Zabbix Agent*.
* *MySQL*, 用户名: `zabbix` 密码: `zabbix`.
* *Monit*  (http://$container_ip:2812, user 'myuser' and password 'mypassword').


## Usage

使用以下命令快速启动一个完整的Zabbix服务：

```
docker run -d -P --name=zabbix  zabbix-in-docker
```

以上命令基于本地镜像 *zabbix-in-docker* 启动了一个名为 *zabbix* 的 *docker* 镜像。通过参数 *-P* 将所有端口映射到宿主机网络。使用命令：`docker ps -f name=zabbix` 查看相应端口映射规则。

如：
```
$ docker ps -f name=zabbix
CONTAINER ID        IMAGE                         COMMAND                CREATED             STATUS              PORTS                                                                                                NAMES
970eb1571545        zabbix-in-docker:latest       "/bin/bash /start.sh   18 hours ago        Up 2 hours          0.0.0.0:49181->10051/tcp, 0.0.0.0:49182->10052/tcp, 0.0.0.0:49183->2812/tcp, 0.0.0.0:49184->80/tcp   zabbix
```

可以通过浏览器带上映射'80'的端口访问 *Zabbix Web UI*.

浏览器打开： `http://<宿主机ip>:<映射到zabbix容器80端口的宿主机端口>/zabbix`

如果你想绑定指定端口到宿主机，则使用以下命令:

```
docker run -d \
           -p 10051:10051 \
           -p 10052:10052 \
           -p 80:80       \
           -p 2812:2812   \
           --name zabbix  \
           zabbix-in-docker
```

以上命令创建了一个名为 `zabbix` 的容器，运行 *Zabbix Server* 通过 *10051* 端口提供服务，通过 *80* 端口可以访问 *Web UI* 。

等待1～2分钟，让容器中所有服务启动完成。通过命令： `docker logs -f $contaienr_id` 查看容器日志。

## 进入 Docker 容器

如果你想对Zabbix进行一些自定义配置或者设计自定义告警脚本，那么你肯定想进入到zabbix容器进行一些直观的操作。_docker exec_ 命令可以实现以 _bash shell_ 交互方式进入容器内部进行操作。

命令格式如下：

```
docker exec -i -t zabbix /bin/bash
```

## Issues and Bugs.

如果你有任何关于本项目的问题，可以在 [这里](https://github.com/berngp/docker-zabbix/issues) 提Issues。

## 使用Dockerfile在本地创建zabbix-in-docker镜像

在一台运行 _Docker_ 的宿主机上, _clone_ 本项目代码，在项目目录下执行 *docker build* 命令创建zabbix-in-docker镜像。

e.g.

```
# 进入zabbix-in-docker目录
cd zabbix-in-docker
# 创建zabbix-in-docker镜像
docker build -t zabbix-in-docker .
# Run it!
docker run -d -P --name=zabbix zabbix-in-docker
```

## Enjoy it!
