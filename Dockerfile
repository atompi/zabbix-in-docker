# Version 2.4.0

FROM centos:centos6
MAINTAINER atompi <atompi@foxmail.com>

# 设置时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 安装 MySQL yum源
RUN yum install -y http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
# 安装 Zabbix yum 源
RUN yum install -y http://repo.zabbix.com/zabbix/2.4/rhel/6/x86_64/zabbix-release-2.4-1.el6.noarch.rpm
# 更新 yum 缓存
RUN yum makecache

# 安装必要工具
RUN yum install -y monit nmap traceroute wget sudo

# 安装 SNMP Utils
# RUN yum -y install libsnmp-dev libsnmp-base libsnmp-dev libsnmp-perl libnet-snmp-perl librrds-perl
RUN yum install -y net-snmp-devel net-snmp-libs net-snmp net-snmp-perl net-snmp-python net-snmp-utils

# 安装 mysql
RUN yum install -y mysql mysql-server

# 安装 Apache、PHP5、ldap、snmp
RUN yum install -y httpd php php-mysql php-snmp php-ldap

# 安装 JDK
RUN yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel

# JDK 环境变量
COPY ./profile.d/java.sh /etc/profile.d/java.sh
RUN chmod 755 /etc/profile.d/java.sh
#RUN /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.25-3.b17.el6_6.x86_64/jre/bin/java

# 安装 Zabbix Server 和 Web UI.
RUN yum install -y zabbix zabbix-agent zabbix-get zabbix-java-gateway zabbix-proxy zabbix-proxy-mysql zabbix-sender zabbix-server zabbix-server-mysql zabbix-web zabbix-web-mysql zabbix22-dbfiles-mysql

# 清理 YUM 缓存
RUN yum clean all && rm -rf /tmp/*

# MySQL 配置
COPY ./mysql/my.cnf /etc/mysql/conf.d/my.cnf
# 获取 tuneup kit
# https://major.io/mysqltuner/
RUN wget http://mysqltuner.pl -O /usr/local/bin/mysqltuner.pl
RUN chmod 755 /usr/local/bin/mysqltuner.pl

COPY ./sudoers.d/zabbix /etc/sudoers.d/zabbix
# Zabbix 配置
COPY ./zabbix/zabbix.ini /etc/php.d/zabbix.ini
COPY ./zabbix/httpd_zabbix.conf /etc/httpd/conf.d/zabbix.conf
COPY ./zabbix/zabbix.conf.php /etc/zabbix/web/zabbix.conf.php
COPY ./zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
COPY ./zabbix/zabbix_java_gateway.conf /etc/zabbix/zabbix_java_gateway.conf
COPY ./zabbix/zabbix_server.conf /etc/zabbix/zabbix_server.conf

RUN chmod 640 /etc/zabbix/zabbix_server.conf
RUN chown root:zabbix /etc/zabbix/zabbix_server.conf

# Monit
ADD ./monitrc /etc/monitrc
RUN chmod 600 /etc/monitrc

# 服务启动脚本
ADD ./scripts/entrypoint.sh /bin/docker-zabbix
RUN chmod 755 /bin/docker-zabbix

# 开放相应端口
# * Zabbix services
# * Apache with Zabbix UI
# * Monit
EXPOSE 10051 10052 80 2812

# 可挂载数据卷
VOLUME ["/var/lib/mysql", "/usr/lib/zabbix/alertscripts", "/usr/lib/zabbix/externalscripts", "/etc/zabbix/zabbix_agentd.d"]

ENTRYPOINT ["/bin/docker-zabbix"]
CMD ["run"]
